package com.uhk.pprobuddysystem.dao;


import com.uhk.pprobuddysystem.model.Subscription;

import java.util.List;

public interface SubscriptionDAO {
    void saveSubscription(Subscription subscription);
    List<Subscription> getAllSubscriptions();
}