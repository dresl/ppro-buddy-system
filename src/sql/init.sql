CREATE TABLE user (
    id INT AUTO_INCREMENT PRIMARY KEY,
    username VARCHAR(255),
    fullname VARCHAR(255),
    type INT,
    age INT,
    faculty VARCHAR(255),
    gender VARCHAR(255),
    agree INT,
    password VARCHAR(255),
    country VARCHAR(255),
    arrival_info_id INT,
    buddy_id INT,
    semester_id INT
);

CREATE TABLE semester (
    id INT AUTO_INCREMENT PRIMARY KEY,
    year INT,
    semester INT
);

CREATE TABLE arrival_info (
    id INT AUTO_INCREMENT PRIMARY KEY,
    place VARCHAR(255),
    date VARCHAR(255)
);

CREATE TABLE event (
    id INT AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR(150),
    description VARCHAR(500),
    semester_id INT
);

CREATE TABLE subscription (
    id INT AUTO_INCREMENT PRIMARY KEY,
    email VARCHAR(150)
);

alter table user add constraint fk_user_buddy foreign key (buddy_id) references user (id);
alter table user add constraint fk_user_semester foreign key (semester_id) references semester (id);
alter table user add constraint fk_user_arrival_info foreign key (arrival_info_id) references arrival_info (id);
alter table event add constraint fk_event_semester foreign key (semester_id) references semester (id);

-- semester
insert into semester (year, semester) values (2023, 0);
insert into semester (year, semester) values (2023, 1);

-- events
insert into event  (title, description, semester_id) values ('Welcome to Czech Republic', '23.2.2024 - This is going to be the best time of your life.', 1);
insert into event  (title, description, semester_id) values ('Party of the year', '12.4.2028 - Enjoy the best party with us!', 1);

-- subscription
insert into subscription (email) values ('dummy@example.com');
insert into subscription (email) values ('dummy2@example.com');

-- arrival_info
insert into arrival_info (place, date) values ('Terminal 1', '21.2.2024');
insert into arrival_info (place, date) values ('Terminal 1', '19.2.2024');
insert into arrival_info (place, date) values ('Terminal 1', '12.1.2024');
insert into arrival_info (place, date) values ('Terminal 2', '15.2.2024');
insert into arrival_info (place, date) values ('Terminal 2', '12.1.2024');
insert into arrival_info (place, date) values ('Terminal 1', '13.1.2024');
insert into arrival_info (place, date) values ('Terminal 2', '13.1.2024');
insert into arrival_info (place, date) values ('Train station', '11.2.2024');

-- buddies
insert into user (username, fullname, age, password, gender, faculty, type, agree) values ('dresl', 'Dominik Resl', 25, 'heslo', 'Male', 'FIM', 0, 1);
insert into user (username, fullname, age, password, gender, faculty, type, agree) values ('mlaugharne1', 'Mikel Laugharne', 19, 'heslo', 'Male', 'FIM', 0, 1);
insert into user (username, fullname, age, password, gender, faculty, type, agree) values ('choulston2', 'Cybill Houlston', 24, 'heslo', 'Male', 'FIM', 0, 1);
insert into user (username, fullname, age, password, gender, faculty, type, agree) values ('tbaddoe3', 'Tamara Baddoe', 22, 'heslo', 'Female', 'FF', 0, 1);
insert into user (username, fullname, age, password, gender, faculty, type, agree) values ('gkersey4', 'Garfield Kersey', 34, 'heslo', 'Male', 'PdF', 0, 1);

-- students with buddies
insert into user (username, fullname, age, country, password, gender, faculty, agree, arrival_info_id, type, buddy_id, semester_id) values ('sstebbing0', 'Selle Stebbing', 24, 'Lithuania', 'heslo', 'Male', 'FIM', 1, 1, 1, 1, 1);
insert into user (username, fullname, age, country, password, gender, faculty, agree, arrival_info_id, type, buddy_id, semester_id) values ('jann1', 'Jo ann Crame', 25, 'Lithuania', 'heslo', 'Male', 'FIM', 1, 8, 1, 1, 1);
insert into user (username, fullname, age, country, password, gender, faculty, agree, arrival_info_id, type, buddy_id, semester_id) values ('mdemkowicz2', 'Morgana Demkowicz', 19, 'Lithuania', 'heslo', 'Male', 'FF', 1, 1, 1, 1, 1);
insert into user (username, fullname, age, country, password, gender, faculty, agree, arrival_info_id, type, buddy_id, semester_id) values ('astokes3', 'Anthony Stokes', 23, 'Lithuania', 'heslo', 'Male', 'FIM', 1, 1, 1, 2, 2);
insert into user (username, fullname, age, country, password, gender, faculty, agree, arrival_info_id, type, buddy_id, semester_id) values ('bbillsberry4', 'Bryon Billsberry', 32, 'Lithuania', 'heslo', 'Male', 'FIM', 1, 6, 1, 3, 1);
insert into user (username, fullname, age, country, password, gender, faculty, agree, arrival_info_id, type, buddy_id, semester_id) values ('malfonzo5', 'Margo Alfonzo', 20, 'USA', 'heslo', 'Male', 'FIM', 1, 1, 1, 4, 1);
insert into user (username, fullname, age, country, password, gender, faculty, agree, arrival_info_id, type, buddy_id, semester_id) values ('jsheringham6', 'Jefferson Sheringham', 26, 'Germany', 'heslo', 'Male', 'FIM', 1, 3, 1, 3, 1);
insert into user (username, fullname, age, country, password, gender, faculty, agree, arrival_info_id, type, buddy_id, semester_id) values ('bshorie7', 'Barbabas Shorie', 25, 'Germany', 'heslo', 'Female', 'FIM', 1, 1, 1, 3, 1);
insert into user (username, fullname, age, country, password, gender, faculty, agree, arrival_info_id, type, buddy_id, semester_id) values ('kbritten8', 'Keary Britten', 35, 'Germany', 'heslo', 'Female', 'FIM', 1, 2, 1, 1, 1);
insert into user (username, fullname, age, country, password, gender, faculty, agree, arrival_info_id, type, buddy_id, semester_id) values ('happleford9', 'Henri Appleford', 18, 'Germany', 'heslo', 'Male', 'FIM', 1, 2, 1, 3, 1);
insert into user (username, fullname, age, country, password, gender, faculty, agree, arrival_info_id, type, buddy_id, semester_id) values ('bottera', 'Balduin Otter', 25, 'Germany', 'heslo', 'Female', 'PdF', 1, 3, 1, 1, 1);
insert into user (username, fullname, age, country, password, gender, faculty, agree, arrival_info_id, type, buddy_id, semester_id) values ('mbernardtb', 'Madelon Bernardt', 24, 'Germany', 'heslo', 'Male', 'FIM', 1, 3, 1, 3, 1);
insert into user (username, fullname, age, country, password, gender, faculty, agree, arrival_info_id, type, buddy_id, semester_id) values ('lpeacopc', 'Linn Peacop', 31, 'Germany', 'heslo', 'Female', 'FF', 1, 1, 1, 4, 2);
insert into user (username, fullname, age, country, password, gender, faculty, agree, arrival_info_id, type, buddy_id, semester_id) values ('labatellid', 'Lynea Abatelli', 20, 'Estonia', 'heslo', 'Male', 'PdF', 1, 1, 1, 1, 2);
insert into user (username, fullname, age, country, password, gender, faculty, agree, arrival_info_id, type, buddy_id, semester_id) values ('dfleye', 'Darill Fley', 22, 'Estonia', 'heslo', 'Male', 'FIM', 1, 1, 1, 2, 2);
-- students without buddies
insert into user (username, fullname, age, country, password, gender, faculty, agree, arrival_info_id, type, semester_id) values ('smonckf', 'Shelia Monck', 34, 'Estonia', 'heslo', 'Female', 'FF', 1, 6, 1, 1);
insert into user (username, fullname, age, country, password, gender, faculty, agree, arrival_info_id, type, semester_id) values ('hresdaleg', 'Heall Resdale', 34, 'Estonia', 'heslo', 'Male', 'FIM', 1, 5, 1, 1);
insert into user (username, fullname, age, country, password, gender, faculty, agree, arrival_info_id, type, semester_id) values ('ebattieh', 'Edlin Battie', 24, 'Estonia', 'heslo', 'Female', 'FF', 1, 4, 1, 1);
insert into user (username, fullname, age, country, password, gender, faculty, agree, arrival_info_id, type, semester_id) values ('navarnei', 'Nola Avarne', 30, 'Finland', 'heslo', 'Male', 'FIM', 1, 4, 1, 2);
insert into user (username, fullname, age, country, password, gender, faculty, agree, arrival_info_id, type, semester_id) values ('aduffieldj', 'Adora Duffield', 25, 'Finland', 'heslo', 'Female', 'FF', 1, 1, 1, 1);
insert into user (username, fullname, age, country, password, gender, faculty, agree, arrival_info_id, type, semester_id) values ('mfernleyk', 'Mildred Fernley', 32, 'Finland', 'heslo', 'Female', 'FIM', 1, 4, 1, 2);
insert into user (username, fullname, age, country, password, gender, faculty, agree, arrival_info_id, type, semester_id) values ('htribel', 'Hannis Tribe', 32, 'Finland', 'heslo', 'Male', 'FIM', 1, 1, 1, 1);
insert into user (username, fullname, age, country, password, gender, faculty, agree, arrival_info_id, type, semester_id) values ('jsherrockm', 'Jarrod Sherrock', 29, 'Taiwan', 'heslo', 'Female', 'FF', 1, 1, 1, 1);
insert into user (username, fullname, age, country, password, gender, faculty, agree, arrival_info_id, type, semester_id) values ('scisecn', 'Stephan Cisec', 26, 'Taiwan', 'heslo', 'Female', 'FIM', 1, 7, 1, 1);
insert into user (username, fullname, age, country, password, gender, faculty, agree, arrival_info_id, type, semester_id) values ('iabbotto', 'Iseabal Abbott', 33, 'Taiwan', 'heslo', 'Female', 'FF', 1, 1, 1, 1);
insert into user (username, fullname, age, country, password, gender, faculty, agree, arrival_info_id, type, semester_id) values ('ekreberp', 'Elizabet Kreber', 24, 'Taiwan', 'heslo', 'Female', 'FIM', 1, 8, 1, 2);
insert into user (username, fullname, age, country, password, gender, faculty, agree, arrival_info_id, type, semester_id) values ('dwattinhamq', 'Dyna Wattinham', 28, 'Taiwan', 'heslo', 'Male', 'PrF', 1, 1, 1, 1);
insert into user (username, fullname, age, country, password, gender, faculty, agree, arrival_info_id, type, semester_id) values ('ocrystalr', 'Oriana Crystal', 22, 'Taiwan', 'heslo', 'Male', 'PrF', 1, 1, 1, 2);
insert into user (username, fullname, age, country, password, gender, faculty, agree, arrival_info_id, type, semester_id) values ('galdersleys', 'Gabriellia Aldersley', 28, 'Poland', 'heslo', 'Male', 'FIM', 1, 2, 1, 1);
insert into user (username, fullname, age, country, password, gender, faculty, agree, arrival_info_id, type, semester_id) values ('rspridgent', 'Roderic Spridgen', 25, 'Poland', 'heslo', 'Male', 'PrF', 1, 3, 1, 1);
