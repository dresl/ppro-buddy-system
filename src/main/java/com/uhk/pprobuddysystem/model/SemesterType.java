package com.uhk.pprobuddysystem.model;

public enum SemesterType {
    SUMMER,
    WINTER
}
