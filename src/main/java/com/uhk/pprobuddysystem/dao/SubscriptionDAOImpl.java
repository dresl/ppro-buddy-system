package com.uhk.pprobuddysystem.dao;

import com.uhk.pprobuddysystem.model.Subscription;
import jakarta.persistence.EntityManager;
import jakarta.persistence.TypedQuery;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class SubscriptionDAOImpl implements SubscriptionDAO {

    private final EntityManager entityManager;

    @Autowired
    public SubscriptionDAOImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    @Transactional
    public void saveSubscription(Subscription subscription) {
        entityManager.persist(subscription);
    }

    @Override
    @Transactional
    public List<Subscription> getAllSubscriptions() {
        TypedQuery<Subscription> query = entityManager.createQuery("FROM Subscription", Subscription.class);
        return query.getResultList();
    }
}
