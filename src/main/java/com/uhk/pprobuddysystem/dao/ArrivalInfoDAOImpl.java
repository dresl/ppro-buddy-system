package com.uhk.pprobuddysystem.dao;

import com.uhk.pprobuddysystem.model.ArrivalInfo;
import com.uhk.pprobuddysystem.model.Semester;
import com.uhk.pprobuddysystem.model.SemesterType;
import com.uhk.pprobuddysystem.model.User;
import jakarta.persistence.EntityManager;
import jakarta.persistence.TypedQuery;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


@Repository
public class ArrivalInfoDAOImpl implements ArrivalInfoDAO {

    private final EntityManager entityManager;

    @Autowired
    public ArrivalInfoDAOImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    @Transactional
    public void saveArrivalInfo(ArrivalInfo arrivalInfo) {
        entityManager.persist(arrivalInfo);
    }

    @Override
    @Transactional
    public void updateArrivalInfo(ArrivalInfo arrivalInfo) {
        ArrivalInfo a = entityManager.find(ArrivalInfo.class, arrivalInfo.getId());
        a.setPlace(arrivalInfo.getPlace());
        a.setDate(arrivalInfo.getDate());
        entityManager.merge(a);
    }
}
