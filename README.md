# ppro-buddy-system

Buddy system made with Spring, thymeleaf and other technologies.

## Good to know
 - `src/sql/init.sql` - init database script with dummy data
 - two UI views - view from Member (buddy) and Exchange Student
 - password for all users is `heslo`
