package com.uhk.pprobuddysystem.dao;

import com.uhk.pprobuddysystem.model.Semester;
import com.uhk.pprobuddysystem.model.User;
import com.uhk.pprobuddysystem.model.UserType;
import jakarta.persistence.EntityManager;
import jakarta.persistence.TypedQuery;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Optional;

@Repository
public class UserDAOImpl implements UserDAO {

    private final EntityManager entityManager;

    @Autowired
    public UserDAOImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public User getUserById(int id) {
        return entityManager.find(User.class, id);
    }

    @Override
    @Transactional
    public Optional<User> getUserByUsername(String username) {
        TypedQuery<User> query = entityManager.createQuery("FROM User WHERE username = ?1", User.class);
        query.setParameter(1, username);
        return query.getResultList().stream().findFirst();
    }

    @Override
    @Transactional
    public void saveUser(User user) {
        entityManager.persist(user);
    }

    @Override
    @Transactional
    public void updateUser(User user) {
        User u = entityManager.find(User.class, user.getId());
        u.setFaculty(user.getFaculty());
        u.setFullName(user.getFullName());
        u.setAge(user.getAge());
        u.setGender(user.getGender());
        u.setArrivalInfo(user.getArrivalInfo());
        u.setCountry(user.getCountry());
        entityManager.merge(u);
    }

    @Override
    @Transactional
    public void deleteUser(int id) {
        User user = entityManager.find(User.class, id);
        entityManager.remove(user);
    }

    @Override
    public List<User> getAllStudentsFromSemester(Semester semester) {
        TypedQuery<User> query = entityManager.createQuery("FROM User WHERE type = ?1 AND semester = ?2", User.class);
        query.setParameter(1, UserType.STUDENT);
        query.setParameter(2, semester);
        return query.getResultList();
    }

    @Override
    public List<User> getAllStudentsFromSemesterWithoutBuddy(Semester semester) {
        List<User> students = this.getAllStudentsFromSemester(semester);
        return students.stream().filter(s -> s.getBuddy() == null).toList();
    }

    @Override
    public List<User> getAllBuddies() {
        TypedQuery<User> query = entityManager.createQuery("FROM User WHERE type = ?1", User.class);
        query.setParameter(1, UserType.MEMBER);
        return query.getResultList();
    }
}