package com.uhk.pprobuddysystem.dao;


import com.uhk.pprobuddysystem.model.ArrivalInfo;
import com.uhk.pprobuddysystem.model.User;

public interface ArrivalInfoDAO {
    void saveArrivalInfo(ArrivalInfo arrivalInfo);

    void updateArrivalInfo(ArrivalInfo arrivalInfo);
}