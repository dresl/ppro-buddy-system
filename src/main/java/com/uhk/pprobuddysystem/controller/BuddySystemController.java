package com.uhk.pprobuddysystem.controller;

import com.uhk.pprobuddysystem.dao.*;
import com.uhk.pprobuddysystem.model.*;
import com.uhk.pprobuddysystem.security.CustomUserDetails;
import jakarta.persistence.NoResultException;
import jakarta.validation.Valid;
import javafx.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.ArrayList;
import java.util.List;

@Controller
public class BuddySystemController {

    private UserDAO userDAO;
    private SemesterDAO semesterDAO;
    private EventDAO eventDAO;
    private SubscriptionDAO subscriptionDAO;

    @Value("${faculties}")
    private List<String> faculties;

    @Value("${genders}")
    private List<String> genders;

    @Value("${countries}")
    private List<String> countries;

    @Autowired
    public BuddySystemController(
            UserDAO userDao,
            SemesterDAO semesterDAO,
            EventDAO eventDAO,
            SubscriptionDAO subscriptionDAO
    ) {
        this.userDAO = userDao;
        this.semesterDAO = semesterDAO;
        this.eventDAO = eventDAO;
        this.subscriptionDAO = subscriptionDAO;
    }

    @GetMapping("/")
    public String allUsers(Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        CustomUserDetails currentPrincipal = (CustomUserDetails) authentication.getPrincipal();
        User user = userDAO.getUserById(currentPrincipal.getUser().getId());
        Semester semester = semesterDAO.getCurrentSemester();
        model.addAttribute("semester", semester);
        if (user.getType() == UserType.STUDENT) {
            return "redirect:/profile";
        } else {
            List<User> students = userDAO.getAllStudentsFromSemesterWithoutBuddy(semester);
            model.addAttribute("students", students);
            return "index";
        }
    }

    @GetMapping("/profile")
    public String profileView(Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        CustomUserDetails currentPrincipal = (CustomUserDetails) authentication.getPrincipal();
        User user = userDAO.getUserById(currentPrincipal.getUser().getId());
        model.addAttribute("user", user);
        if (user.getType() == UserType.MEMBER) {
            Semester sem = semesterDAO.getCurrentSemester();
            List<User> assignedStudents = user.getAssignedStudents().stream().filter(i -> i.getSemester() == sem).toList();
            model.addAttribute("assignedStudents", assignedStudents);
            return "buddy-profile";
        } else {
            Semester semester = semesterDAO.getCurrentSemester();
            model.addAttribute("semester", semester);
            return "student-profile";
        }
    }

    @GetMapping("/profile/{id}")
    public String profileView(Model model, @PathVariable int id){
        User user = userDAO.getUserById(id);
        model.addAttribute("user", user);
        if (user.getType() == UserType.MEMBER) {
            Semester sem = semesterDAO.getCurrentSemester();
            List<User> assignedStudents = user.getAssignedStudents().stream().filter(i -> i.getSemester() == sem).toList();
            model.addAttribute("assignedStudents", assignedStudents);
            return "buddy-profile";
        } else {
            return "student-profile";
        }
    }

    // Buddy controllers

    @GetMapping("/register/buddy")
    public String registerBuddy(Model model){
        model.addAttribute("user", new User());
        model.addAttribute("faculties", faculties);
        model.addAttribute("genders", genders);
        return "buddy-register";
    }

    @PostMapping("/register/buddy")
    public String registerBuddyProcess(@Valid @ModelAttribute("user") User user, BindingResult br, Model model) {
        model.addAttribute("faculties", faculties);
        model.addAttribute("genders", genders);
        if (br.hasErrors()) {
            return "buddy-register";
        }
        if (userDAO.getUserByUsername(user.getUsername()).isPresent()) {
            model.addAttribute("globalError", "This username is already taken.");
            return "buddy-register";
        }
        user.setType(UserType.MEMBER);
        userDAO.saveUser(user);
        return "redirect:/login";
    }

    @GetMapping("/update/buddy/{id}")
    public String updateBuddy(Model model, @PathVariable int id) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        CustomUserDetails currentPrincipal = (CustomUserDetails) authentication.getPrincipal();
        if (currentPrincipal.getUser().getId() != id) {
            return "redirect:/";
        }
        try {
            User user = userDAO.getUserById(id);
            model.addAttribute("user", user);
            model.addAttribute("faculties", faculties);
            model.addAttribute("genders", genders);
            return "buddy-update";
        } catch (Exception e) {
            return "error";
        }
    }

    @PostMapping("/update/buddy/{id}")
    public String updateBuddyProcess(@Valid @ModelAttribute("user") User user, BindingResult br, Model model) {
        if (br.hasErrors()) {
            model.addAttribute("faculties", faculties);
            model.addAttribute("genders", genders);
            return "buddy-update";
        }
        user.setType(UserType.MEMBER);
        userDAO.updateUser(user);
        return "redirect:/profile";
    }

    @PostMapping("/buddy/take-student/{id}")
    public String updateBuddyProcess(@PathVariable int id) {
        User student = userDAO.getUserById(id);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        CustomUserDetails currentPrincipal = (CustomUserDetails) authentication.getPrincipal();
        User buddy = userDAO.getUserById(currentPrincipal.getUser().getId());
        buddy.addStudent(student);
        userDAO.saveUser(student);
        return "redirect:/profile";
    }

    // Student controllers

    @GetMapping("/register/student")
    public String registerStudent(Model model){
        model.addAttribute("user", new User());
        model.addAttribute("faculties", faculties);
        model.addAttribute("genders", genders);
        model.addAttribute("countries", countries);
        return "student-register";
    }

    @PostMapping("/register/student")
    public String registerStudentProcess(
            @Valid @ModelAttribute("user") User user,
            BindingResult br,
            Model model
    ) {
        if (br.hasErrors()) {
            model.addAttribute("faculties", faculties);
            model.addAttribute("genders", genders);
            model.addAttribute("countries", countries);
            return "student-register";
        }
        user.setType(UserType.STUDENT);
        Semester semester = semesterDAO.getCurrentSemester();
        user.setSemester(semester);
        userDAO.saveUser(user);
        return "redirect:/login";
    }

    @GetMapping("/update/student/{id}")
    public String updateStudent(Model model, @PathVariable int id) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        CustomUserDetails currentPrincipal = (CustomUserDetails) authentication.getPrincipal();
        if (currentPrincipal.getUser().getId() != id) {
            return "redirect:/";
        }
        try {
            User user = userDAO.getUserById(id);
            model.addAttribute("user", user);
            model.addAttribute("faculties", faculties);
            model.addAttribute("genders", genders);
            model.addAttribute("countries", countries);
            return "student-update";
        } catch (Exception e) {
            return "error";
        }
    }

    @PostMapping("/update/student/{id}")
    public String updateStudentProcess(@Valid @ModelAttribute("user") User user, BindingResult br, Model model) {
        if (br.hasErrors()) {
            model.addAttribute("faculties", faculties);
            model.addAttribute("genders", genders);
            model.addAttribute("countries", countries);
            return "student-update";
        }
        user.setType(UserType.MEMBER);
        userDAO.updateUser(user);
        return "redirect:/profile";
    }

    // other views

    @GetMapping("/all-buddies")
    public String allBuddies(Model model) {
        Semester semester = semesterDAO.getCurrentSemester();
        model.addAttribute("semester", semester);
        model.addAttribute("buddies", userDAO.getAllBuddies());
        return "buddies-list";
    }

    @GetMapping("/statistics")
    public String statistics(Model model) {
        Semester semester = semesterDAO.getCurrentSemester();
        List<User> students = userDAO.getAllStudentsFromSemester(semester);
        model.addAttribute("students", students);
        model.addAttribute("semester", semester);
        model.addAttribute("countries", countries);
        model.addAttribute("buddies", userDAO.getAllBuddies());
        List<Pair<String, Long>> countriesAgg = new ArrayList<>();
        for (String country : countries) {
            Pair p = new Pair<String, Long>(country, students.stream().filter(s -> s.getCountry().equals(country)).count());
            countriesAgg.add(p);
        }
        model.addAttribute("countriesAgg", countriesAgg);
        return "statistics";
    }

    // subscriptions

    @GetMapping("/subscriptions")
    public String subscriptionList(Model model) {
        List<Subscription> subscriptions = subscriptionDAO.getAllSubscriptions();
        model.addAttribute("subscriptions", subscriptions);
        return "subscription-list";
    }

    @PostMapping("/subscriptions/create")
    public String subscriptionsCreate(@RequestParam("email") String email) {
        Subscription subscription = new Subscription();
        subscription.setEmail(email);
        subscriptionDAO.saveSubscription(subscription);
        return "redirect:/";
    }


    // events

    @GetMapping("/event-list")
    public String eventList(Model model) {
        Semester semester = semesterDAO.getCurrentSemester();
        List<Event> events = eventDAO.getAllEventsFromSemester(semester);
        model.addAttribute("events", events);
        model.addAttribute("semester", semester);
        return "event-list";
    }

    @GetMapping("/events/create")
    public String createEvent(Model model){
        model.addAttribute("event", new Event());
        return "event-create";
    }

    @PostMapping("/events/create")
    public String createEventProcess(
            @Valid @ModelAttribute("event") Event event,
            BindingResult br,
            Model model
    ) {
        if (br.hasErrors()) {
            return "event-create";
        }
        Semester semester = semesterDAO.getCurrentSemester();
        event.setSemester(semester);
        eventDAO.saveEvent(event);
        return "redirect:/event-list";
    }

    @PostMapping("/events/delete/{id}")
    public String deleteEvent(@PathVariable int id) {
        eventDAO.deleteEvent(id);
        return "redirect:/event-list";
    }

    // authentication

    @GetMapping("/login")
    public String login(Authentication auth, RedirectAttributes redirectAttrs) {
        if (auth != null) {
            redirectAttrs.addFlashAttribute("error", "User is already logged in!");
            return "redirect:/";
        }
        return "login";
    }

    @InitBinder
    public void initBinder(WebDataBinder db){
        StringTrimmerEditor e = new StringTrimmerEditor(true);
        db.registerCustomEditor(String.class, e);
    }
}