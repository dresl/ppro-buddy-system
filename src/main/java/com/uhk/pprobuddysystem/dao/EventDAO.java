package com.uhk.pprobuddysystem.dao;


import com.uhk.pprobuddysystem.model.Semester;
import com.uhk.pprobuddysystem.model.Event;
import com.uhk.pprobuddysystem.model.User;

import java.util.List;

public interface EventDAO {
    void saveEvent(Event event);
    List<Event> getAllEventsFromSemester(Semester semester);
    void deleteEvent(int id);
}