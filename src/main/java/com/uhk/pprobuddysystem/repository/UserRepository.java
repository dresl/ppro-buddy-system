package com.uhk.pprobuddysystem.repository;


import com.uhk.pprobuddysystem.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


/**
 * This interface is created for proper working with spring security package
 * */
public interface UserRepository extends JpaRepository<User,Long> {
    @Query("SELECT u FROM User u WHERE u.username = ?1")
    public User findByUsername(String username);

}