package com.uhk.pprobuddysystem.dao;

import com.uhk.pprobuddysystem.model.Event;
import com.uhk.pprobuddysystem.model.Semester;
import com.uhk.pprobuddysystem.model.User;
import com.uhk.pprobuddysystem.model.UserType;
import jakarta.persistence.EntityManager;
import jakarta.persistence.TypedQuery;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class EventDAOImpl implements EventDAO {

    private final EntityManager entityManager;

    @Autowired
    public EventDAOImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    @Transactional
    public void saveEvent(Event event) {
        entityManager.persist(event);
    }

    @Override
    public List<Event> getAllEventsFromSemester(Semester semester) {
        TypedQuery<Event> query = entityManager.createQuery("FROM Event WHERE semester = ?1", Event.class);
        query.setParameter(1, semester);
        return query.getResultList();
    }

    @Override
    @Transactional
    public void deleteEvent(int id) {
        Event event = entityManager.find(Event.class, id);
        entityManager.remove(event);
    }
}