FROM eclipse-temurin:17-jdk
COPY out/artifacts/ppro_buddy_system_jar/ppro-buddy-system.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]