package com.uhk.pprobuddysystem.dao;


import com.uhk.pprobuddysystem.model.Semester;
import com.uhk.pprobuddysystem.model.User;

import java.util.List;
import java.util.Optional;

public interface UserDAO {
    User getUserById(int id);
    void saveUser(User user);
    void updateUser(User user);
    Optional<User> getUserByUsername(String username);
    void deleteUser(int id);
    List<User> getAllStudentsFromSemester(Semester semester);
    List<User> getAllStudentsFromSemesterWithoutBuddy(Semester semester);
    List<User> getAllBuddies();
}