package com.uhk.pprobuddysystem.model;

public enum UserType {
    MEMBER,
    STUDENT
}
