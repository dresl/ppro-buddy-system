package com.uhk.pprobuddysystem.dao;

import com.uhk.pprobuddysystem.model.Semester;


public interface SemesterDAO {
    Semester getCurrentSemester();
}