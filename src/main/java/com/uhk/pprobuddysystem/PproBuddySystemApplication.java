package com.uhk.pprobuddysystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PproBuddySystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(PproBuddySystemApplication.class, args);
    }

}
