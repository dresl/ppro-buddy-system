package com.uhk.pprobuddysystem.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Size(min = 4, message = "Fill at least 4 characters")
    @Column(name = "password")
    private String password;

    @NotNull(message = "Required")
    @Size(min = 4, message = "Fill at least 4 characters")
    @Column(name = "username")
    private String username;

    @NotNull(message = "Required")
    @Size(min = 1, message = "Fill at least one character")
    @Column(name = "fullname")
    private String fullName;

    @Min(value = 18, message = "Minimum is 18")
    @Column(name = "age")
    private int age;

    @Column(name = "faculty")
    private String faculty;

    @Column(name = "gender")
    private String gender = "Male";

    @Column(name = "agree")
    @AssertTrue(message = "You have to agree with terms of use.")
    private boolean agree;

    @Column(name = "type")
    private UserType type;

    @Column(name = "country")
    private String country;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "buddy")
    private List<User> assignedStudents;

    @ManyToOne
    @JoinColumn(name = "buddy_id")
    private User buddy;

    @ManyToOne
    @JoinColumn(name = "semester_id")
    private Semester semester;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "arrival_info_id")
    private ArrivalInfo arrivalInfo;

    public void addStudent(User student){
        if (assignedStudents == null)
            assignedStudents = new ArrayList<>();
        student.setBuddy(this);
        assignedStudents.add(student);
    }

    public User getBuddy() {
        return buddy;
    }

    public void setBuddy(User buddy) {
        this.buddy = buddy;
    }

    public List<User> getAssignedStudents() {
        return assignedStudents;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public boolean getAgree() {
        return agree;
    }

    public void setAgree(boolean agree) {
        this.agree = agree;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public UserType getType() {
        return type;
    }

    public void setType(UserType type) {
        this.type = type;
    }

    public ArrivalInfo getArrivalInfo() {
        return arrivalInfo;
    }

    public void setArrivalInfo(ArrivalInfo arrivalInfo) {
        this.arrivalInfo = arrivalInfo;
    }

    public Semester getSemester() {
        return semester;
    }

    public void setSemester(Semester semester) {
        this.semester = semester;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", fullName='" + fullName + '\'' +
                ", faculty='" + faculty + '\'' +
                ", gender='" + gender + '\'' +
                ", agree=" + agree +
                ", type='" + type + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id && Objects.equals(password, user.password) && Objects.equals(username, user.username);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, password, username);
    }
}