package com.uhk.pprobuddysystem.dao;

import com.uhk.pprobuddysystem.model.Semester;
import com.uhk.pprobuddysystem.model.SemesterType;
import jakarta.persistence.EntityManager;
import jakarta.persistence.TypedQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


@Repository
public class SemesterDAOImpl implements SemesterDAO {

    private final EntityManager entityManager;

    @Autowired
    public SemesterDAOImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    /*
    * This functions returns current semester based on the current time, but for testing purpose
    * there is set 2023 summer semester by default
    * */
    @Override
    public Semester getCurrentSemester() {
        TypedQuery<Semester> query = entityManager.createQuery("FROM Semester WHERE year = ?1 AND semester = ?2", Semester.class);
        // query.setParameter(1, Year.now().getValue());
        // FOR TESTING PURPOSE
        query.setParameter(1, 2023);
        query.setParameter(2, SemesterType.SUMMER);
        return query.getSingleResult();
    }
}
